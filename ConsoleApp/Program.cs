using Challenge;
using Challenge.DataContracts;
using System;
using System.IO;
using System.Collections.Generic;
namespace ConsoleApp
{
    class Program
    {
        private static void TestPreviousTasks(ChallengeClient challengeClient)
        {
            var allTasks = challengeClient.GetAllTasksAsync().Result;
            for (int i = 0; i < allTasks.Count; i++)
            {
                var task = allTasks[i];
                var typeId = task.TypeId;
                if (typeId == "polynomial-root" || typeId == "starter"
                    || task.Status != TaskStatus.Success)
                    continue;
                var answer = GetAnswer(task);
                if (answer == "no roots" && task.TeamAnswer == "no roots")
                    continue;
                if (answer != task.TeamAnswer)
                {
                    Console.WriteLine(answer);
                    Console.WriteLine(task.TeamAnswer);
                }
            }
            Console.WriteLine("Решение прошло все тесты из предыдущих раундов");
        }

        private static void AutoTest(string round, ChallengeClient challengeClient)
        {
            while (true)
            {
                var newTask = challengeClient.AskNewTaskAsync(round).Result;
                var answer = GetAnswer(newTask);
                var updatedTask = challengeClient.CheckTaskAnswerAsync(newTask.Id, answer).Result;
                if (updatedTask.Status == TaskStatus.Success)
                    Console.WriteLine($"Ура! Ответ угадан!");
                else if (updatedTask.Status == TaskStatus.Failed)
                {
                    Console.WriteLine($"Похоже ответ не подошел и задачу больше сдать нельзя...");
                    break;
                }
            }
        }

        private static string GetAnswer(TaskResponse task)
        {
            var type = task.TypeId;
            var question = task.Question;
            if (type == "math")
                return Solver.MathExpression.Calculate(question);
            if (type == "determinant")
                return Solver.Determinant.GetDeterminant(question);
            if (type == "polynomial-root")
                return Solver.PolynomialRoot.GetRoot(question);
            if (type == "cypher")
                return Solver.Cypher.GetAnswer(question);
            if (type == "shape")
                return Solver.ShapeFigure.GetShape(question);
            if (type == "string-number")
                return Solver.StringNumber.GetNumber(question);
            if (type == "inverse-matrix")
                return Solver.InverseMatrix.GetInverseMatrix(question);
            if (type == "json")
                return Solver.Json.GetSumOfJsonObject(question);
            else
                throw new System.IO.InvalidDataException("WrongType or Не сделали ещё");
        }

        static void Challenge()
        {
            const string teamSecret = "020bLv18/ScQLAx/QzybtassSZMtFCzJ";
            if (string.IsNullOrEmpty(teamSecret))
            {
                Console.WriteLine("Задай секрет своей команды, чтобы можно было делать запросы от ее имени");
                return;
            }
            var challengeClient = new ChallengeClient(teamSecret);

            const string challengeId = "projects-course";
            //Console.WriteLine($"Нажми ВВОД, чтобы получить информацию о челлендже {challengeId}");
            //Console.ReadLine();
            var challenge = challengeClient.GetChallengeAsync(challengeId).Result;
            //Console.WriteLine(challenge.Description);
            //Console.WriteLine();

            TestPreviousTasks(challengeClient);

            string round = challenge.Rounds[2].Id;

            //Console.WriteLine("Вы хотите запустить автотест? Если да, то напишите: '1352467980', иначе нажмите ВВОД");
            //if (Console.ReadLine() == "1352467980")
            //AutoTest(round, challengeClient);

            Console.WriteLine($"Нажми ВВОД, чтобы получить задачу");
            Console.ReadLine();
            var newTask = challengeClient.AskNewTaskAsync(round).Result;
            Console.WriteLine($"  Новое задание, статус {newTask.Status}");
            Console.WriteLine($"  Формулировка: {newTask.UserHint}");
            Console.WriteLine($"{newTask.Question}");

            Console.WriteLine(newTask.TypeId);
            Console.WriteLine("Ответ правильный?");
            string answer = GetAnswer(newTask);
            Console.WriteLine(answer);
            Console.ReadLine();

            Console.WriteLine($"Нажми ВВОД, чтобы ответить на полученную задачу самым правильным ответом: {answer}");
            Console.ReadLine();
            var updatedTask = challengeClient.CheckTaskAnswerAsync(newTask.Id, answer).Result;

            if (updatedTask.Status == TaskStatus.Success)
                Console.WriteLine($"Ура! Ответ угадан!");
            else if (updatedTask.Status == TaskStatus.Failed)
                Console.WriteLine($"Похоже ответ не подошел и задачу больше сдать нельзя...");            
        }

        static void Main(string[] args)
        {
            Challenge();

            //var question = "Possible answers=ellipse,rectangle,triangle|(241,237) (235,273) (229,285) (232,232) (196,293) (218,225) (176,283) (159,207) (147,232) (244,255) (240,262) (136,252) (206,219) (240,263) (163,199) (221,299) (137,263) (232,279) (166,199) (185,288) (246,239) (192,291) (212,222) (221,227) (210,301) (141,243) (141,244) (230,283) (180,285) (225,229) (195,293) (248,247) (161,276) (177,284) (144,237) (222,297) (239,236) (238,267) (145,236) (151,224) (172,202) (164,198) (171,201) (221,300) (191,211) (149,228) (236,271) (234,274) (203,217) (241,260) (231,232) (237,268) (135,262) (150,226) (230,231) (181,206) (243,238) (223,296) (133,258) (238,235) (153,220) (173,282) (214,303) (153,272) (165,198) (158,210) (154,272) (146,234) (158,274) (197,214) (237,235) (190,290) (156,273) (182,286) (132,261) (214,223) (183,207) (227,288) (200,216) (140,265) (174,282) (231,281) (250,241) (222,227) (207,219) (194,213) (248,240) (234,275) (142,242) (247,249) (246,251) (219,225) (163,200) (243,256) (199,215) (151,223) (179,285) (170,201) (138,264) (140,245) (167,199) (156,214) (196,214) (188,289) (169,200) (136,263) (158,209) (170,280) (134,257) (234,233) (167,279) (159,208) (220,226) (188,210) (226,289) (173,202) (171,281) (150,225) (176,204) (140,246) (216,224) (141,265) (145,267) (242,258) (152,221) (134,256) (157,274) (177,204) (142,241) (246,250) (143,240) (153,219) (184,208) (160,205) (147,269) (247,248) (163,277) (198,294) (181,286) (186,288) (160,206) (220,301) (242,237) (143,266) (133,259) (219,304) (217,304) (206,299) (209,300) (180,206) (192,212) (139,264) (232,278) (157,211) (161,203) (203,297) (195,213) (236,270) (224,294) (197,294) (227,230) (233,277) (249,241) (133,261) (134,262) (174,203) (166,278) (200,295) (201,216) (250,243) (162,201) (152,271) (184,287) (194,292) (225,292) (244,238) (190,211) (149,227) (239,265) (132,260) (162,202) (229,284) (178,205) (215,223) (248,246) (226,229) (210,221) (142,266) (238,266) (211,301) (225,291) (212,302) (243,257) (213,302) (218,305) (137,250) (205,298) (187,289) (168,200) (162,276) (217,224) (143,239) (148,230) (209,220) (229,231) (202,217) (154,217) (179,205) (175,203) (139,248) (187,209) (186,209) (205,218) (224,293) (215,303) (227,287) (182,207) (189,290) (178,284) (224,228) (201,296) (137,251) (185,208) (208,220) (216,304) (160,275) (208,300) (226,290) (193,212) (207,299) (241,261) (222,298) (228,286) (183,287) (228,230) (175,283) (249,244) (230,282) (155,273) (236,234) (231,280) (139,247) (199,295) (152,222) (154,218) (233,233) (233,276) (150,270) (146,268) (146,233) (135,255) (235,234) (148,229) (157,212) (135,254) (219,303) (202,297) (245,252) (144,238) (147,231) (220,302) (211,221) (149,270) (144,267) (239,264) (165,278) (240,236) (223,228) (168,279) (223,295) (189,210) (169,280) (172,281) (242,259) (198,215) (164,277) (151,271) (213,222) (161,204) (244,254) (193,292) (245,239) (156,213) (155,215) (235,272) (204,218) (138,249) (191,291) (148,269) (245,253) (155,216) (145,235) (237,269) (136,253) (159,275) (249,245) (247,240) (250,242) (204,298)";
            //var type = "shape";
            //var task = new TaskResponse();
            //task.Question = question;
            //task.TypeId = type;
            //var answer = GetAnswer(task);
            //Console.WriteLine(answer);

            //var text = "looux'bxb0hehxjr'dovxethpfhexo7xdrh'bx9rfb'fvx'vjxfrhl'vjx7ro xhddxboxfv7hrvox'xjr'dovxuhhthriexdafjh";
            //var text2 = "Look at these Dragon Species of Great Britain and Ireland From Egg to Inferno A Dragon Keeper's Guide";
            //Solver.Cypher.TestFoo(text, text2);
            //var b = Solver.Cypher.CaesarCode(text);
            //Console.WriteLine(b);
            //Console.WriteLine(answer);
            Console.ReadKey();
        }
    }
}
