﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Numerics;
using System.Drawing;
using System.IO;
using Accord.Math;
using MathNet.Numerics.LinearAlgebra;
using Newtonsoft.Json;

namespace Solver
{
    class MathExpression
    {
        private static List<string> operators = new List<string> { "(", ")", "+", "-", "*", "/", "^", "%" };

        private static bool IsOperator(string symbol)
        {
            return operators.Contains(symbol);
        }

        private static int GetPriority(string symbol)
        {
            switch (symbol)
            {
                case "(": return 0;
                case ")": return 0;
                case "+": return 1;
                case "-": return 1;
                case "*": return 2;
                case "%": return 2;
                case "/": return 2;
                case "^": return 3;
                default: return 4;
            }
        }

        private static IEnumerable<string> Separate(string input)
        {
            int pos = 0;
            while (pos < input.Length)
            {
                string s = string.Empty + input[pos];
                if (!IsOperator(input[pos].ToString()))
                {
                    if (char.IsDigit(input[pos]) || input[pos] == 'i')
                        for (int i = pos + 1; i < input.Length &&
                            (char.IsDigit(input[i]) || input[i] == 'i' || input[i] == ',' || input[i] == '.'); i++)
                            s += input[i];
                    else if (char.IsLetter(input[pos]))
                        for (int i = pos + 1; i < input.Length &&
                            (char.IsLetter(input[i]) || char.IsDigit(input[i])); i++)
                            s += input[i];
                }
                yield return s;
                pos += s.Length;
            }
        }

        private static string[] GetExpression(string input)
        {
            List<string> outputSeparated = new List<string>();
            Stack<string> stack = new Stack<string>();
            foreach (var c in Separate(input))
            {
                if (IsOperator(c))
                {
                    if (stack.Count > 0 && !c.Equals("("))
                    {
                        if (c.Equals(")"))
                        {
                            string s = stack.Pop();
                            while (s != "(")
                            {
                                outputSeparated.Add(s);
                                s = stack.Pop();
                            }
                        }
                        else if (GetPriority(c) > GetPriority(stack.Peek()))
                            stack.Push(c);
                        else
                        {
                            while (stack.Count > 0 && GetPriority(c) <= GetPriority(stack.Peek()))
                                outputSeparated.Add(stack.Pop());
                            stack.Push(c);
                        }
                    }
                    else
                        stack.Push(c);
                }
                else
                    outputSeparated.Add(c);
            }
            if (stack.Count > 0)
                foreach (var c in stack)
                    outputSeparated.Add(c);
            return outputSeparated.ToArray();
        }

        private static string Counting(string[] input)
        {
            var result = new Complex(0, 0);
            var temp = new Stack<Complex>();

            for (var i = 0; i < input.Length; i++)
            {
                if (IsOperator(input[i]))
                {
                    var a = temp.Pop();
                    var b = temp.Pop();

                    var ar = (long)a.Real;
                    var ai = (long)a.Imaginary;
                    var br = (long)b.Real;
                    var bi = (long)b.Imaginary;
                    switch (input[i])
                    {
                        case "+": result = b + a; break;
                        case "-": result = b - a; break;
                        case "*": result = new Complex(ar * br - ai * bi, ar * bi + ai * br); break;
                        case "%": result = new Complex(br % ar, 0); break;
                        case "/": result = new Complex((ar * br + ai * bi) / (ar * ar + ai * ai), (ai * br - ar * bi) / (ar * ar + ai * ai)); break;
                        case "^":
                            result = new Complex((long)Math.Pow(br, ar), 0);
                            break;
                    }
                    temp.Push(result);
                }
                else
                {
                    Complex number;
                    var len = input[i].Length;
                    if (input[i][len - 1] == 'i')
                        number = new Complex(0, long.Parse(input[i].Substring(0, len - 1)));
                    else
                        number = new Complex(long.Parse(input[i]), 0);
                    temp.Push(number);
                }
            }
            var num = temp.Peek();
            var real = (long)num.Real;
            var imaginary = (long)num.Imaginary;
            if (imaginary == 0)
                return real.ToString();
            if (imaginary > 0)
                return real + "+" + imaginary + "i";
            return real + "" + imaginary + "i";
        }

        //private static string Counting(string[] input)
        //{
        //    long result = 0;
        //    var temp = new Stack<long>();

        //    for (var i = 0; i < input.Length; i++)
        //    {
        //        if (IsOperator(input[i]))
        //        {
        //            var a = temp.Pop();
        //            var b = temp.Pop();

        //            switch (input[i])
        //            {
        //                case "+": result = b + a; break;
        //                case "-": result = b - a; break;
        //                case "*": result = b * a; break;
        //                case "%": result = b % a; break;
        //                case "/": result = b / a; break;
        //                case "^":
        //                    result = (long)Math.Pow(b, a);
        //                    break;
        //            }
        //            temp.Push(result);
        //        }
        //        else
        //            temp.Push(long.Parse(input[i]));
        //    }
        //    return temp.Peek().ToString();
        ////}

        public static string Calculate(string input)
        {
            var output = GetExpression(input); //Перевод выражения в обратную польскую запись
            var result = Counting(output); //Решаем
            return result;
        }
    }

    class Determinant
    {
        public static void Test()
        {
            var time1 = new Stopwatch();
            var time2 = new Stopwatch();
            long resultTime1 = 0;
            long resultTime2 = 0;
            var rand = new Random();
            for (var k = 0; k < 1000; k++)
            {
                const int n = 8;
                var a = new List<List<double>>(n);
                var b = new double[n, n];
                var c = new double[n][];
                for (int i = 0; i < n; i++)
                {
                    a.Add(new List<double>(n));
                    c[i] = new double[n];
                    for (int j = 0; j < n; j++)
                    {
                        var r = rand.Next(-80, 80);
                        a[i].Add(r);
                        b[i, j] = r;
                        c[i][j] = r;
                    }
                }
                time1.Start();
                var res1 = MyGauss(a);
                time1.Stop();
                time2.Start();
                var res2 = GetDeterminantWithMathNet(b);
                if (res1 != res2)
                {
                    for (var ai = 0; ai < n; ai++)
                    {
                        for (var aj = 0; aj < n; aj++)
                            Console.Write($"{b[ai, aj],4}");
                        Console.WriteLine();
                    }
                    Console.WriteLine("Упс, ебать");
                }
            }
            Console.WriteLine(resultTime1);
            Console.WriteLine(resultTime2);
            Console.WriteLine("Всё классно");
        }

        private static string GetDeterminantWithMathNet(double[,] a)
        {
            var width = a.GetLength(0);
            var height = a.GetLength(1);
            if (width != height)
                throw new System.IO.InvalidDataException($"Кол-во строк и столбцов: {width}, {height}");
            var M = Matrix<double>.Build;
            var m = M.DenseOfArray(a);
            var det = m.Determinant();
            det = Math.Round(det);
            return ((long)det).ToString();
        }

        public static string GetDeterminant(string line)
        {
            var matrixEl = line.Split('\\', '&', ' ')
                               .Where(e => !string.IsNullOrEmpty(e))
                               .Select(e => int.Parse(e))
                               .ToArray();
            var period = (int)Math.Sqrt(matrixEl.Length);
            if (period * period != matrixEl.Length)
                throw new Exception("WrongPeriod");
            var m = new double[period, period];
            var m2 = new BigInteger[period][];
            var matrix = new List<List<double>>(period);
            for (var i = 0; i < period; ++i)
            {
                m2[i] = new BigInteger[period];
                matrix.Add(new List<double>(period));
                for (var j = 0; j < period; ++j)
                {
                    matrix[i].Add(matrixEl[i * period + j]);
                    m[i, j] = matrixEl[i * period + j];
                    m2[i][j] = new BigInteger(matrixEl[i * period + j]);
                }
            }
            //var res = MyGauss(matrix);
            var res1 = GetM(m2).ToString();
            //var res2 = GetDeterminantWithMathNet(m);
            return res1;
        }

        private static void PrintArray(List<List<double>> a)
        {
            var n = a.Count();
            for (var ai = 0; ai < n; ai++)
            {
                for (var aj = 0; aj < n; aj++)
                    Console.Write($"{a[ai][aj],5}");
                Console.WriteLine();
            }
            Console.WriteLine();
        }

        private static string MyGauss(List<List<double>> a)
        {
            const double EPS = (double)(1E-14);
            double det = 1;
            var n = a.Count;
            for (var i = 0; i < n; ++i)
            {
                var iMax = i;
                for (var j = i + 1; j < n; ++j)
                    if (Math.Abs(a[j][i]) > Math.Abs(a[iMax][i]))
                        iMax = j;
                if (Math.Abs(a[iMax][i]) < EPS)
                {
                    det = 0;
                    break;
                }
                for (var l = 0; l < n; ++l)
                {
                    var temp = a[i][l];
                    a[i][l] = a[iMax][l];
                    a[iMax][l] = temp;
                }

                det *= (i != iMax) ? -1 : 1;
                det *= a[i][i];
                for (var j = i + 1; j < n; ++j)
                {
                    var q = -a[j][i] / a[i][i];
                    for (var k = n - 1; k >= i; --k)
                        a[j][k] += q * a[i][k];
                }
            }
            return ((long)Math.Round(det)).ToString();
        }

        private static BigInteger GetM(BigInteger[][] matrix)
        {
            BigInteger det;
            if (matrix.Length == 2)
                det = matrix[0][0] * matrix[1][1] - matrix[0][1] * matrix[1][0];
            else
            {
                var minor = new BigInteger[matrix.Length - 1][]; //минор, но возможно, что это матрица n-ого порядка
                int i, j, k;
                var sign = 1;
                det = 0;
                for (i = 0; i < matrix.Length; i++)
                {
                    for (j = 1; j < matrix.Length; j++) //сохранем значения для "возможно" минора
                    {
                        minor[j - 1] = new BigInteger[matrix.Length - 1];
                        for (k = 0; k < i; k++) //значения до диагонали
                            minor[j - 1][k] = matrix[j][k];

                        for (k++; k < matrix.Length; k++)   //значения после диагонали
                            minor[j - 1][k - 1] = matrix[j][k];
                    }

                    var temp = GetM(minor);
                    temp = matrix[0][i] * sign * temp;
                    det += temp;

                    if (sign > 0)
                        sign = -1;
                    else
                        sign = 1;
                }
            }
            return det;
        }
    }

    class PolynomialRoot
    {
        private static double[] parameters = new double[31];
        private static int maxPower = -1;

        private static double GetParam(string a)
        {
            a = (a[0] == '(') ? a.Substring(1, a.Length - 2) : a;
            return double.Parse(a, System.Globalization.CultureInfo.InvariantCulture);
        }

        private static void GetParams(string[] line)
        {
            foreach (var e in line)
            {
                var power = 0;
                var ind = e.IndexOf("^");
                if (ind != -1)
                    power = int.Parse(e.Substring(ind + 1, e.Length - ind - 1));
                else if (e[e.Length - 1] == 'x')
                {
                    power = 1;
                    ind = e.Length;
                }
                else
                    ind = e.Length + 2;

                if (power > maxPower)
                    maxPower = power;
                if (e[0] == 'x')
                    parameters[power] += 1;
                else
                    parameters[power] += GetParam(e.Substring(0, ind - 2));
            }
        }

        private static string Solve(double x0)
        {
            var eps = 1e-15;
            double x1 = x0 - Fx(x0) / Dfx(x0); // первое приблжение
            var amountIterations = 0;
            while (Math.Abs(x1 - x0) > eps)
            {
                x0 = x1;
                x1 = x1 - Fx(x1) / Dfx(x1); // последующие приближения
                amountIterations++;
                if (amountIterations > 1000)
                    return "no roots";
            }
            if (Fx(x1) >= 1e-10)
                throw new Exception("wrong answer");
            return x1.ToString().Replace(",", ".");
        }

        private static double Fx(double x) // вычисляемая функция
        {
            var result = parameters[maxPower];
            for (var i = maxPower - 1; i >= 0; i--)
            {
                result = result * x + parameters[i];
            }
            return result;
        }

        private static double Dfx(double x) // производная функции
        {
            var result = parameters[maxPower] * maxPower;
            for (var i = maxPower - 1; i > 0; i--)
            {
                result = result * x + parameters[i] * i;
            }
            return result;
        }

        private static double FindRoot(double a, double b)
        {
            var epsilon = 1e-9;
            while (Math.Abs(b - a) > epsilon)
            {
                a = b - (b - a) * Fx(b) / (Fx(b) - Fx(a));
                //if (Math.Abs(b - a) > epsilon)
                //    break;
                b = a + (a - b) * Fx(a) / (Fx(a) - Fx(b));
            }
            if (Fx(b) >= epsilon)
                throw new Exception("Wrong answer");
            if (double.IsNaN(b))
                throw new Exception("None");
            return b;
        }

        public static string GetRoot(string exp)
        {
            parameters = new double[31];
            maxPower = -1;
            var splitExp = exp.Split(' ', '+')
                              .Where(e => !string.IsNullOrEmpty(e))
                              .ToArray();
            GetParams(splitExp);
            if (parameters[0] == 0)
                return "0";
            //if (maxPower > 2)
            return Solve(1.5);
            //var a = parameters[2];
            //var b = parameters[1];
            //var c = parameters[0];
            //if (a == 0)
            //{
            //    if (b == 0)
            //    {
            //        if (c == 0)
            //            return "0";
            //        return "no roots";
            //    }
            //    return (-c / b).ToString().Replace(",", ".");
            //}
            //var D = b * b - 4 * a * c;
            //return (D < 0) ? "no roots" : ((-b + Math.Sqrt(D)) / (2 * a)).ToString().Replace(",", ".");
        }
    }
    
    class Cypher
    {
        public static void TestFoo(string text1, string text2)
        {
            text2 = text2.ToLower();
            var s = new List<string>();
            var alphabet = "abcdefghijklmnopqrstuvwxyz0123456789' ";
            //var temp = alphabet.ToCharArray();
            //Array.Reverse(temp);
            //alphabet = new string(temp);
            var result1 = new string[text1.Length];
            var result2 = new string[text1.Length];
            for (var i = 0; i < text1.Length; i++)
            {
                var find1 = alphabet.IndexOf(text1[i]);
                var find2 = alphabet.IndexOf(text2[i]);
                result1[i] = find1.ToString();
                result2[i] = find2.ToString();
                var t = text2[i] + " : " + text1[i] + "  " + (find1 - find2);
                if (s.IndexOf(t) == -1)
                    s.Add(t);
            }
            var len = 50;
            var l = text1.Length;
            //for (var i = 0; i < len; i++)
            //    Console.Write($"{text1[i], -3}");
            //Console.WriteLine();
            //for (var i = 0; i < len; i++)
            //    Console.Write($"{result1[i], -3}");
            //Console.WriteLine();
            //for (var i = len; i < l; i++)
            //    Console.Write($"{text1[i],-3}");
            //Console.WriteLine();
            //for (var i = len; i < l; i++)
            //    Console.Write($"{result1[i],-3}");
            //Console.WriteLine();
            //Console.WriteLine();
            //for (var i = 0; i < len; i++)
            //    Console.Write($"{text2[i], -3}");
            //Console.WriteLine();
            //for (var i = 0; i < len; i++)
            //    Console.Write($"{result2[i], -3}");
            //Console.WriteLine();
            //for (var i = len; i < l; i++)
            //    Console.Write($"{text2[i],-3}");
            //Console.WriteLine();
            //for (var i = len; i < l; i++)
            //    Console.Write($"{result2[i],-3}");
            s.Sort();
        }

        private static string CaesarCode(string key, string text)
        {
            var alphabet = "abcdefghijklmnopqrstuvwxyz0123456789' ".ToCharArray();
            var result = new StringBuilder();

            var shift = int.Parse(key) % 38;
            for (var i = 0; i < text.Length; ++i)
                {
                    int ind;
                    if (text[i] == ' ')
                        ind = (37 - shift + 38) % 38;
                    else if (text[i] == '\'')
                        ind = (36 - shift + 38) % 38;
                    else if (char.IsDigit(text[i]))
                        ind = (26 + text[i] - '0' - shift + 38) % 38;
                    else
                        ind = (text[i] - 'a' - shift + 38) % 38;
                    result.Append(alphabet[ind]);
                }
            return result.ToString();
        }

        private static void HeyHarry(List<string> result, List<string> words, string sentence, string word, char space)
        {
            var mask = sentence.Split(space)
                .Where(e => !string.IsNullOrEmpty(e))
                .Select(e => e.Length)
                .ToArray();
            int countLeft = 0, countRight = 0, pos = 0;
            var lenMask = mask.Length;
            for (var i = 0; i < lenMask; i++)
                if (mask[i] == word.Length)
                {
                    countLeft = i;
                    pos = i;
                    countRight = mask.Length - i - 1;
                    break;
                }

            var len = words.Count();
            var answer = new StringBuilder();
            for (var i = 0; i < len; i++)
            {
                if (word != words[i] || i < countLeft || len - i - 1 < countRight)
                    continue;

                var isAnswer = true;
                for (var j = 0; j < lenMask; j++)
                {
                    if (words[j + i - countLeft].Length != mask[j])
                    {
                        isAnswer = false;
                        break;
                    }
                    answer.Append(words[j + i - countLeft]);
                    if (j != lenMask - 1)
                        answer.Append(" ");
                }
                if (isAnswer && !result.Contains(answer.ToString()))
                    result.Add(answer.ToString());
                answer.Clear();
            }
        }

        private static string LongestWord(string word, string text)
        {
            var words = JsonConvert.DeserializeObject<List<string>>(File.ReadAllText(@"..\..\HarryWords.txt"));
            var result = new List<string>();
            var lenWord = word.Length;
            var lenText = text.Length;
            var reviewedSpaces = new HashSet<char>();
            if (text.IndexOf(text[lenWord], 0, lenWord) == -1)
                HeyHarry(result, words, text, word, text[lenWord]);
            for (var i = 0; i < lenText - lenWord - 1; i++)
                if (text[i] == text[i + lenWord + 1] && text.IndexOf(text[i], i + 1, lenWord - 1) == -1)
                    HeyHarry(result, words, text, word, text[i]);
            if (text.IndexOf(text[lenText - lenWord - 1], lenText - lenWord, lenWord) == -1)
                HeyHarry(result, words, text, word, text[lenText - lenWord - 1]);

            if (result.Count != 1)
                throw new Exception("Что-то пошло не так");
            return result[0];
        }

        public static string GetAnswer(string input)
        {
            var splitText = input.Split('=', '|');
            if (splitText[0] == "Caesar's code")
                return CaesarCode(splitText[1], splitText[2]);
            else if (splitText[0] == "first longest word")
                return LongestWord(splitText[1], splitText[2]);
            else
                throw new Exception("Не умею такое дешифровать");
        }
    }

    class ShapeFigure
    {
        private static bool pointsAreEqual(Point a, Point b)
        {
            var eps = 5.0;
            var ax = (double)a.X;
            var ay = (double)a.Y;
            var bx = (double)b.X;
            var by = (double)b.Y;
            return (ax - bx) * (ax - bx) + (ay - by) * (ay - by) <= eps * eps;
        }

        private static void UpdatePoints(Point[] points, Point p)
        {
            if (p.X > points[0].X || p.X == points[0].X && p.Y < points[0].Y)
            {
                points[0].X = p.X;
                points[0].Y = p.Y;
            }
            if (p.X < points[1].X || p.X == points[1].X && p.Y > points[1].Y)
            {
                points[1].X = p.X;
                points[1].Y = p.Y;
            }
            if (p.Y > points[2].Y || p.Y == points[2].Y && p.X > points[2].X)
            {
                points[2].X = p.X;
                points[2].Y = p.Y;
            }
            if (p.Y < points[3].Y || p.Y == points[3].Y && p.X < points[3].X)
            {
                points[3].X = p.X;
                points[3].Y = p.Y;
            }
        }

        private static double DistancePointToStraight(Point a, Point b, Point c)
        {
            return Math.Abs((b.Y - a.Y) * c.X - (b.X - a.X) * c.Y + b.X * a.Y - b.Y * a.X)
                / Math.Sqrt((b.Y - a.Y) * (b.Y - a.Y) + (b.X - a.X) * (b.X - a.X));
        }

        private static string GetAnswer(Point[] points, string input)
        {
            if (points[0].Y == points[3].Y && points[3].X == points[1].X && points[1].Y == points[2].Y && points[2].X == points[0].X)
                return "rectangle";
            for (var i = 0; i < 3; i++)
                for (var j = i + 1; j < 4; j++)
                {
                    var p1 = points[i];
                    var p2 = points[j];
                    if (pointsAreEqual(p1, p2) || p1.X == p2.X || p1.Y == p2.Y)
                        return "triangle";
                }
            var a = points[1];
            var b = points[2];
            if (a.X == b.X)
                return "rectangle";
            var countPointsUnderLine = 0;
            var splitInput = input.Split(' ');
            for (var i = 0; i < splitInput.Length; ++i)
            {
                var point = splitInput[i].Split(',', '(', ')')
                                    .Where(s => !string.IsNullOrEmpty(s))
                                    .Select(s => int.Parse(s))
                                    .ToArray();
                var c = new Point(point[0], point[1]);
                var distance = DistancePointToStraight(a, b, c);
                if ((double)(c.Y - b.Y) / (a.Y - b.Y) < (double)(c.X - b.X) / (a.X - b.X)
                    && distance > 2)
                    countPointsUnderLine++;
            }
            if (countPointsUnderLine > 10)
                return "ellipse";
            return "rectangle";
        }

        private static void CountPoints(Point[] points, string input)
        {
            var splitInput = input.Split(' ');
            for (var i = 0; i < splitInput.Length; ++i)
            {
                var point = splitInput[i].Split(',', '(', ')')
                                    .Where(s => !string.IsNullOrEmpty(s))
                                    .Select(s => int.Parse(s))
                                    .ToArray();
                UpdatePoints(points, new Point(point[0], point[1]));
            }
        }

        public static string GetShape(string input)
        {
            var splitInput = input.Split('|');
            var type = "-1";
            if (splitInput[0] == "Possible answers=cirle,square,equilateraltriangle")
                type = "1";
            else if (splitInput[0] == "Possible answers=ellipse,rectangle,triangle")
                type = "2";
            else
                throw new Exception("Неизвестный тип");
            var translate = new Dictionary<string, string> { { "ellipse", "cirle" }, { "rectangle", "square" }, { "triangle", "equilateraltriangle" } };
            var points = new Point[4];
            points[1].X = int.MaxValue;
            points[3].Y = int.MaxValue;
            CountPoints(points, splitInput[1]);
            var answer = GetAnswer(points, splitInput[1]);
            if (type == "1")
                answer = translate[answer];
            return answer;
        }
    }

    //class ShapeFigure
    //{
    //    private static void UpdatePoints(int[,] points, int x, int y)
    //    {
    //        if (x > points[0, 0])
    //        {
    //            points[0, 0] = x;
    //            points[0, 1] = 1;
    //        }
    //        else if (x == points[0, 0])
    //            points[0, 1]++;

    //        if (x < points[1, 0])
    //        {
    //            points[1, 0] = x;
    //            points[1, 1] = 1;
    //        }
    //        else if (x == points[1, 0])
    //            points[1, 1]++;

    //        if (y > points[2, 0])
    //        {
    //            points[2, 0] = y;
    //            points[2, 1] = 1;
    //        }
    //        else if (y == points[2, 0])
    //            points[2, 1]++;

    //        if (y < points[3, 0])
    //        {
    //            points[3, 0] = y;
    //            points[3, 1] = 1;
    //        }
    //        else if (y == points[3, 0])
    //            points[3, 1]++;
    //    }

    //    private static string GetAnswer2(int[,] a, int count)
    //    {
    //        var xDif = a[0, 0] - a[1, 0] + 1;
    //        var yDif = a[2, 0] - a[3, 0] + 1;
    //        var squareS = 1.0 * xDif * yDif;
    //        var triangleS = Math.Sqrt(3) / 4 * Math.Pow(Math.Max(xDif, yDif), 2);
    //        var circleS = (xDif / 2) * (xDif / 2) * Math.PI;
    //        squareS = Math.Abs(count - squareS);
    //        triangleS = Math.Abs(count - triangleS);
    //        circleS = Math.Abs(count - circleS);
    //        var minS = Math.Min(Math.Min(squareS, triangleS), circleS);
    //        var answer = "";
    //        if (squareS == minS)
    //            answer = "square";
    //        else if (triangleS == minS)
    //            answer = "equilateraltriangle";
    //        else if (circleS == minS)
    //            answer = "cirle";
    //        return answer;
    //    }

    //    private static string GetAnswer(int[,] result)
    //    {
    //        var answer = "cirle";
    //        if (result[0, 0] - result[1, 0] == result[2, 0] - result[3, 0])
    //        {
    //            if (result[0, 1] == result[0, 0] - result[1, 0] + 1)
    //                answer = "square";
    //            else if (result[0, 1] == result[1, 1] && result[1, 1] == result[2, 1] && result[2, 1] == result[3, 1])
    //                answer = "cirle";
    //        }
    //        else
    //        {
    //            var a = Math.Max(result[0, 0] - result[1, 0], result[2, 0] - result[3, 0]) + 1;
    //            var count = 0;
    //            for (var i = 0; i < 4; ++i)
    //                if (a == result[i, 1])
    //                    count++;
    //            if (count == 1)
    //                answer = "equilateraltriangle";
    //        }
    //        return answer;
    //    }

    //    private static int CountPoints(int[,] p, string input)
    //    {
    //        var points = input.Split(' ');
    //        for (var i = 0; i < points.Length; ++i)
    //        {
    //            var temp = points[i].Split(',', '(', ')')
    //                                .Where(s => !string.IsNullOrEmpty(s))
    //                                .Select(s => int.Parse(s))
    //                                .ToArray();
    //            UpdatePoints(p, temp[0], temp[1]);
    //        }
    //        return points.Length;
    //    }

    //    public static string GetShape(string input)
    //    {
    //        var splitInput = input.Split('|');
    //        if (splitInput[0] != "Possible answers=cirle,square,equilateraltriangle")
    //            throw new Exception("Неизвестный тип");
    //        var points = new int[4, 2];
    //        points[1, 0] = int.MaxValue;
    //        points[3, 0] = int.MaxValue;
    //        var count = CountPoints(points, splitInput[1]);
    //        var answer1 = GetAnswer(points);
    //        var answer2 = GetAnswer2(points, count);
    //        if (answer1 != answer2)
    //            throw new Exception("Wrong answer");
    //        return answer1;
    //    }
    //}

    class StringNumber
    {
        public static string GetNumber(string input)
        {
            var big = new Dictionary<string, double> { { "quintillion", 1e18 }, { "quadrillion", 1e15 }, { "trillion", 1e12 }, {"billion", 1e9 }, { "million", 1e6 }, { "thousand", 1e3 } };
            var small = new Dictionary<string, double> {
                { "zero", 0 }, { "one", 1 }, { "two", 2 }, { "three", 3 }, { "four", 4}, { "five", 5 }, { "six", 6 },
                { "seven", 7 }, { "eight", 8 }, { "nine", 9 }, { "ten", 10 }, { "eleven", 11 }, { "twelve", 12 },
                { "thirteen", 13 }, { "fourteen", 14 }, { "fifteen", 15 }, { "sixteen", 16 }, { "seventeen", 17 }, { "eighteen", 18 }, { "nineteen", 19 },
                { "twenty", 20 }, { "thirty", 30 }, { "forty", 40 }, { "fifty", 50 }, { "sixty", 60 }, { "seventy", 70 }, { "eighty", 80 }, { "ninety", 90 }, { "hundred", 100} };
            var splitInput = input.Split(' ', '-');
            BigInteger result = 0;
            BigInteger tempNum = 0;
            for (var i = 0; i < splitInput.Length; i++)
            {
                var num = splitInput[i];
                if (num == "hundred")
                    tempNum *= 100;
                else if (big.ContainsKey(num))
                {
                    result += BigInteger.Max(tempNum, 1) * (BigInteger)big[num];
                    tempNum = 0;
                }
                else
                    tempNum += (BigInteger)small[num];
            }
            result += tempNum;
            return result.ToString();
        }
    }

    class InverseMatrix
    {
        private static double[,] GetMatrixFromInput(string input)
        {
            var rows = input.Split('\\')
                            .Where(e => !string.IsNullOrEmpty(e))
                            .ToArray();
            var height = rows.Length;
            var width = rows[0].Split('&').Length;
            var matrix = new double[height, width];
            for (var i = 0; i < height; i++)
            {
                var row = rows[i].Split('&', ' ')
                    .Where(e => !string.IsNullOrEmpty(e))
                    .Select(e => int.Parse(e))
                    .ToArray();
                for (var j = 0; j < width; j++)
                    matrix[i, j] = row[j];
            }
            return matrix;
        }

        private static string GetMatrixToOutput(double[,] matrix)
        {
            var height = matrix.GetLength(0);
            var width = matrix.GetLength(1);
            var result = new StringBuilder();
            for (var i = 0; i < height; i++)
            {
                for (var j = 0; j < width; j++)
                {
                    result.Append(matrix[i, j]);
                    if (j != width - 1)
                        result.Append(" & ");
                }
                if (i != height - 1)
                    result.Append(@" \\ ");
            }
            return result.ToString().Replace(',', '.');
        }

        public static string GetInverseMatrix(string input)
        {
            var matrix = GetMatrixFromInput(input);
            double[,] inverseMatrix;
            try
            {
                inverseMatrix = Matrix.Inverse(matrix);
            }
            catch
            {
                return "unsolvable";
            }
            return GetMatrixToOutput(inverseMatrix);
        }
    }

    class Json
    {
        public static string GetSumOfJsonObject(string input)
        {
            var splitInput = input.Split('{', '}', ',').Where(e => !string.IsNullOrEmpty(e)).ToArray();
            var len = splitInput.Length;
            var result = 0;
            for (var i = 0; i < len; i++)
            {
                var element = splitInput[i].Split(':');
                if (string.IsNullOrEmpty(element[1]))
                    continue;
                result += int.Parse(element[1]);
            }
            return result.ToString();
        }
    }
}